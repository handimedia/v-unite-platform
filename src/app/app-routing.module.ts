import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EntranceComponent } from './components/entrance/entrance.component';
import { BoardroomsComponent } from './components/boardrooms/boardrooms.component';
import { ChatroomsComponent } from './components/chatrooms/chatrooms.component';
import { LobbysComponent } from './components/lobbys/lobbys.component';

const routes: Routes = [
  { path: '', redirectTo: '/lobbys', pathMatch: 'full'},
  { path: 'entrance', component: EntranceComponent},
  { path: 'boardrooms', component: BoardroomsComponent},
  { path: 'chatrooms', component: ChatroomsComponent},
  { path: 'lobbys', component: LobbysComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
