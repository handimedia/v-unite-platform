import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HashLocationStrategy, LocationStrategy  } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EntranceComponent } from './components/entrance/entrance.component';
import { BoardroomsComponent } from './components/boardrooms/boardrooms.component';
import { ChatroomsComponent } from './components/chatrooms/chatrooms.component';
import { LobbysComponent } from './components/lobbys/lobbys.component';

@NgModule({
  declarations: [
    AppComponent,
    EntranceComponent,
    BoardroomsComponent,
    ChatroomsComponent,
    LobbysComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [{provide : LocationStrategy , useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
