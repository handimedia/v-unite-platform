import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardroomsComponent } from './boardrooms.component';

describe('BoardroomsComponent', () => {
  let component: BoardroomsComponent;
  let fixture: ComponentFixture<BoardroomsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardroomsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardroomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
