import { HostListener, Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

@Component({
  selector: 'app-boardrooms',
  templateUrl: './boardrooms.component.html',
  styleUrls: ['./boardrooms.component.scss']
})
export class BoardroomsComponent implements OnInit {

  heroImage: string = 'assets/boardrooms/1.jpg';
  scale: any = 1;
  vh: any = window.innerHeight;
  vw: any = window.innerWidth;
  boardroom: any = 1;

  constructor(
    private _sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
     this.scale = Math.min(event.target.innerWidth/this.vw, event.target.innerHeight/this.vh);
     console.log(this.scale);
  }

  get transform(): SafeStyle {
    return this._sanitizer.bypassSecurityTrustStyle("translate(-50%, -50%) scale("+ this.scale + ")");
  }

  changeBoardroom(number){
    this.boardroom = number;
    this.heroImage = 'assets/boardrooms/'+ number +'.jpg';
  }

}
