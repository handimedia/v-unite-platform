import { HostListener, Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

@Component({
  selector: 'app-entrance',
  templateUrl: './entrance.component.html',
  styleUrls: ['./entrance.component.scss']
})
export class EntranceComponent implements OnInit {

  heroImage: string = 'assets/entrance-bg.jpg';
  scaleMode: boolean = true;
  scale: any = 1;
  vh: any = window.innerHeight;
  vw: any = window.innerWidth;
  constructor(
    private _sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
     this.scale = Math.min(event.target.innerWidth/this.vw, event.target.innerHeight/this.vh);
     console.log(this.scale);
  }

  get transform(): SafeStyle {
    return this._sanitizer.bypassSecurityTrustStyle("translate(-50%, -50%) scale("+ this.scale + ")");
  }

}
