(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<router-outlet></router-outlet>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/boardrooms/boardrooms.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/boardrooms/boardrooms.component.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"hero\" [style.backgroundImage]=\"'url('+ heroImage +')'\"\n [style.transform]=\"transform\"\n [style.width.px]=\"vw\"\n [style.height.px]=\"vh\">\n  <span id=\"logo\" class=\"\" [className]=\"'boardroom-' + boardroom\"></span>\n  <button id=\"back\" [className]=\"'boardroom-' + boardroom\">\n    <span>Back to Lobby</span>\n    <div class=\"icon\">\n      <i class=\"fa fa-caret-left\" aria-hidden=\"true\"></i>\n    </div>\n  </button>\n  <button id=\"watch\" [className]=\"'boardroom-' + boardroom\">\n    <div class=\"icon\">\n      <i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i>\n    </div>\n    <span>Watch Now</span>\n  </button>\n  <ul class=\"nav\">\n    <li *ngFor=\"let number of [1,2,3,4,5]\"><span class=\"circle-icon\" [class.active]=\"boardroom == number\" (click)=\"changeBoardroom(number)\"></span></li>\n  </ul>\n</div>\n<div class=\"logos\" [style.top.px]=\"vh\">\n  <div class=\"logo\" *ngFor=\"let number of [1,2,3,4,5,6,7,8,9]\">\n    <img src=\"assets/logo{{number}}.jpg\" alt=\"\">\n  </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/chatrooms/chatrooms.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/chatrooms/chatrooms.component.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"hero\" [style.backgroundImage]=\"'url('+ heroImage +')'\"\n [style.transform]=\"transform\"\n [style.width.px]=\"vw\"\n [style.height.px]=\"vh\">\n  <span id=\"logo\" class=\"\" [className]=\"'boardroom-' + boardroom\"></span>\n  <button id=\"back\" [className]=\"'boardroom-' + boardroom\">\n    <span>Back to Lobby</span>\n    <div class=\"icon\">\n      <i class=\"fa fa-caret-left\" aria-hidden=\"true\"></i>\n    </div>\n  </button>\n  <button id=\"watch\" [className]=\"'boardroom-' + boardroom\">\n    <div class=\"icon\">\n      <i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i>\n    </div>\n    <span>Watch Now</span>\n  </button>\n  <ul class=\"nav\">\n    <li *ngFor=\"let number of [1,2,3,4]\"><span class=\"circle-icon\" [class.active]=\"boardroom == number\" (click)=\"changeBoardroom(number)\"></span></li>\n  </ul>\n</div>\n<div class=\"logos\" [style.top.px]=\"vh\">\n  <div class=\"logo\" *ngFor=\"let number of [1,2,3,4,5,6,7,8,9]\">\n    <img src=\"assets/logo{{number}}.jpg\" alt=\"\">\n  </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/entrance/entrance.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/entrance/entrance.component.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"hero\" [style.backgroundImage]=\"'url('+ heroImage +')'\"\n [style.transform]=\"transform\"\n [style.width.px]=\"vw\"\n [style.height.px]=\"vh\" *ngIf=\"scaleMode\">\n  <span id=\"logo\"></span>\n  <button class=\"main\">enter</button>\n  <!-- <button (click)=\"scaleMode=true\" *ngIf=\"!scaleMode\">Enable Scale Mode</button>\n  <button (click)=\"scaleMode=false\" *ngIf=\"scaleMode\">Disble Scale Mode</button> -->\n</div>\n<div class=\"hero cover\" *ngIf=\"!scaleMode\" [style.backgroundImage]=\"'url('+ heroImage +')'\">\n  <span class=\"logo\"></span>\n  <button class=\"main\">enter</button>\n  <button (click)=\"scaleMode=true\" *ngIf=\"!scaleMode\">Enable Scale Mode</button>\n  <button (click)=\"scaleMode=false\" *ngIf=\"scaleMode\">Disble Scale Mode</button>\n</div>\n<div class=\"logos\"  [style.top.px]=\"vh\">\n  <div class=\"logo\" *ngFor=\"let number of [1,2,3,4,5,6,7,8,9]\">\n    <img src=\"assets/logo{{number}}.jpg\" alt=\"\">\n  </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/lobbys/lobbys.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/lobbys/lobbys.component.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"hero\" [style.backgroundImage]=\"'url('+ heroImage +')'\"\n [style.transform]=\"transform\"\n [style.width.px]=\"vw\"\n [style.height.px]=\"vh\">\n  <span id=\"logo\" class=\"\" [className]=\"'lobby-' + lobby\"></span>\n  <ul class=\"nav\">\n    <li *ngFor=\"let number of [1,2,3,4]\"><span class=\"circle-icon\" [class.active]=\"lobby == number\" (click)=\"changeBoardroom(number)\"></span></li>\n  </ul>\n</div>\n<div class=\"logos\" [style.top.px]=\"vh\">\n  <div class=\"logo\" *ngFor=\"let number of [1,2,3,4,5,6,7,8,9]\">\n    <img src=\"assets/logo{{number}}.jpg\" alt=\"\">\n  </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault, __classPrivateFieldGet, __classPrivateFieldSet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldGet", function() { return __classPrivateFieldGet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldSet", function() { return __classPrivateFieldSet; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}

function __classPrivateFieldGet(receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
}

function __classPrivateFieldSet(receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
}


/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _components_entrance_entrance_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/entrance/entrance.component */ "./src/app/components/entrance/entrance.component.ts");
/* harmony import */ var _components_boardrooms_boardrooms_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/boardrooms/boardrooms.component */ "./src/app/components/boardrooms/boardrooms.component.ts");
/* harmony import */ var _components_chatrooms_chatrooms_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/chatrooms/chatrooms.component */ "./src/app/components/chatrooms/chatrooms.component.ts");
/* harmony import */ var _components_lobbys_lobbys_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/lobbys/lobbys.component */ "./src/app/components/lobbys/lobbys.component.ts");







const routes = [
    { path: '', redirectTo: '/lobbys', pathMatch: 'full' },
    { path: 'entrance', component: _components_entrance_entrance_component__WEBPACK_IMPORTED_MODULE_3__["EntranceComponent"] },
    { path: 'boardrooms', component: _components_boardrooms_boardrooms_component__WEBPACK_IMPORTED_MODULE_4__["BoardroomsComponent"] },
    { path: 'chatrooms', component: _components_chatrooms_chatrooms_component__WEBPACK_IMPORTED_MODULE_5__["ChatroomsComponent"] },
    { path: 'lobbys', component: _components_lobbys_lobbys_component__WEBPACK_IMPORTED_MODULE_6__["LobbysComponent"] }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'v-unite-platform';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_entrance_entrance_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/entrance/entrance.component */ "./src/app/components/entrance/entrance.component.ts");
/* harmony import */ var _components_boardrooms_boardrooms_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/boardrooms/boardrooms.component */ "./src/app/components/boardrooms/boardrooms.component.ts");
/* harmony import */ var _components_chatrooms_chatrooms_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/chatrooms/chatrooms.component */ "./src/app/components/chatrooms/chatrooms.component.ts");
/* harmony import */ var _components_lobbys_lobbys_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/lobbys/lobbys.component */ "./src/app/components/lobbys/lobbys.component.ts");










let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
            _components_entrance_entrance_component__WEBPACK_IMPORTED_MODULE_6__["EntranceComponent"],
            _components_boardrooms_boardrooms_component__WEBPACK_IMPORTED_MODULE_7__["BoardroomsComponent"],
            _components_chatrooms_chatrooms_component__WEBPACK_IMPORTED_MODULE_8__["ChatroomsComponent"],
            _components_lobbys_lobbys_component__WEBPACK_IMPORTED_MODULE_9__["LobbysComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"]
        ],
        providers: [{ provide: _angular_common__WEBPACK_IMPORTED_MODULE_3__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_3__["HashLocationStrategy"] }],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/components/boardrooms/boardrooms.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/components/boardrooms/boardrooms.component.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#logo {\n  position: absolute;\n  left: 50%;\n  transform: translateX(-50%);\n}\n#logo.boardroom-1 {\n  top: 20%;\n}\n#logo.boardroom-2 {\n  top: 18%;\n}\n#logo.boardroom-3 {\n  top: 27%;\n  margin-left: 15px;\n}\n#logo.boardroom-4 {\n  top: 18%;\n  width: 400px;\n  height: 200px;\n}\n#logo.boardroom-5 {\n  top: 12%;\n}\nbutton#back.boardroom-4 {\n  right: 65px;\n  left: auto;\n}\nbutton#watch.boardroom-2 {\n  left: auto;\n  right: 11.5%;\n}\nbutton#watch.boardroom-3 {\n  top: 20%;\n  left: 6.5%;\n}\nbutton#watch.boardroom-4 {\n  left: 11.5%;\n}\nbutton#watch.boardroom-5 {\n  left: 4%;\n  top: 21%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uaWtrby92LXVuaXRlLXBsYXRmb3JtL3NyYy9hcHAvY29tcG9uZW50cy9ib2FyZHJvb21zL2JvYXJkcm9vbXMuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvYm9hcmRyb29tcy9ib2FyZHJvb21zLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsMkJBQUE7QUNDRjtBREFFO0VBQ0UsUUFBQTtBQ0VKO0FEQUU7RUFDRSxRQUFBO0FDRUo7QURBRTtFQUNFLFFBQUE7RUFDQSxpQkFBQTtBQ0VKO0FEQUU7RUFDRSxRQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUNFSjtBREFFO0VBQ0UsUUFBQTtBQ0VKO0FESUk7RUFDRSxXQUFBO0VBQ0EsVUFBQTtBQ0ROO0FES0k7RUFDRSxVQUFBO0VBQ0EsWUFBQTtBQ0hOO0FES0k7RUFDRSxRQUFBO0VBQ0EsVUFBQTtBQ0hOO0FES0k7RUFDRSxXQUFBO0FDSE47QURLSTtFQUNFLFFBQUE7RUFDQSxRQUFBO0FDSE4iLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2JvYXJkcm9vbXMvYm9hcmRyb29tcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNsb2dve1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpO1xuICAmLmJvYXJkcm9vbS0xe1xuICAgIHRvcDogMjAlO1xuICB9XG4gICYuYm9hcmRyb29tLTJ7XG4gICAgdG9wOiAxOCU7XG4gIH1cbiAgJi5ib2FyZHJvb20tM3tcbiAgICB0b3A6IDI3JTtcbiAgICBtYXJnaW4tbGVmdDogMTVweDtcbiAgfVxuICAmLmJvYXJkcm9vbS00e1xuICAgIHRvcDogMTglO1xuICAgIHdpZHRoOiA0MDBweDtcbiAgICBoZWlnaHQ6IDIwMHB4O1xuICB9XG4gICYuYm9hcmRyb29tLTV7XG4gICAgdG9wOiAxMiU7XG4gIH1cbn1cblxuYnV0dG9ue1xuICAmI2JhY2t7XG4gICAgJi5ib2FyZHJvb20tNHtcbiAgICAgIHJpZ2h0OiA2NXB4O1xuICAgICAgbGVmdDogYXV0bztcbiAgICB9XG4gIH1cbiAgJiN3YXRjaHtcbiAgICAmLmJvYXJkcm9vbS0ye1xuICAgICAgbGVmdDogYXV0bztcbiAgICAgIHJpZ2h0OiAxMS41JTtcbiAgICB9XG4gICAgJi5ib2FyZHJvb20tM3tcbiAgICAgIHRvcDogMjAlO1xuICAgICAgbGVmdDogNi41JTtcbiAgICB9XG4gICAgJi5ib2FyZHJvb20tNHtcbiAgICAgIGxlZnQ6IDExLjUlO1xuICAgIH1cbiAgICAmLmJvYXJkcm9vbS01e1xuICAgICAgbGVmdDogNCU7XG4gICAgICB0b3A6IDIxJTtcbiAgICB9XG4gIH1cbn1cbiIsIiNsb2dvIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKTtcbn1cbiNsb2dvLmJvYXJkcm9vbS0xIHtcbiAgdG9wOiAyMCU7XG59XG4jbG9nby5ib2FyZHJvb20tMiB7XG4gIHRvcDogMTglO1xufVxuI2xvZ28uYm9hcmRyb29tLTMge1xuICB0b3A6IDI3JTtcbiAgbWFyZ2luLWxlZnQ6IDE1cHg7XG59XG4jbG9nby5ib2FyZHJvb20tNCB7XG4gIHRvcDogMTglO1xuICB3aWR0aDogNDAwcHg7XG4gIGhlaWdodDogMjAwcHg7XG59XG4jbG9nby5ib2FyZHJvb20tNSB7XG4gIHRvcDogMTIlO1xufVxuXG5idXR0b24jYmFjay5ib2FyZHJvb20tNCB7XG4gIHJpZ2h0OiA2NXB4O1xuICBsZWZ0OiBhdXRvO1xufVxuYnV0dG9uI3dhdGNoLmJvYXJkcm9vbS0yIHtcbiAgbGVmdDogYXV0bztcbiAgcmlnaHQ6IDExLjUlO1xufVxuYnV0dG9uI3dhdGNoLmJvYXJkcm9vbS0zIHtcbiAgdG9wOiAyMCU7XG4gIGxlZnQ6IDYuNSU7XG59XG5idXR0b24jd2F0Y2guYm9hcmRyb29tLTQge1xuICBsZWZ0OiAxMS41JTtcbn1cbmJ1dHRvbiN3YXRjaC5ib2FyZHJvb20tNSB7XG4gIGxlZnQ6IDQlO1xuICB0b3A6IDIxJTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/components/boardrooms/boardrooms.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/boardrooms/boardrooms.component.ts ***!
  \***************************************************************/
/*! exports provided: BoardroomsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BoardroomsComponent", function() { return BoardroomsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");



let BoardroomsComponent = class BoardroomsComponent {
    constructor(_sanitizer) {
        this._sanitizer = _sanitizer;
        this.heroImage = 'assets/boardrooms/1.jpg';
        this.scale = 1;
        this.vh = window.innerHeight;
        this.vw = window.innerWidth;
        this.boardroom = 1;
    }
    ngOnInit() {
    }
    onResize(event) {
        this.scale = Math.min(event.target.innerWidth / this.vw, event.target.innerHeight / this.vh);
        console.log(this.scale);
    }
    get transform() {
        return this._sanitizer.bypassSecurityTrustStyle("translate(-50%, -50%) scale(" + this.scale + ")");
    }
    changeBoardroom(number) {
        this.boardroom = number;
        this.heroImage = 'assets/boardrooms/' + number + '.jpg';
    }
};
BoardroomsComponent.ctorParameters = () => [
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:resize', ['$event'])
], BoardroomsComponent.prototype, "onResize", null);
BoardroomsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-boardrooms',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./boardrooms.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/boardrooms/boardrooms.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./boardrooms.component.scss */ "./src/app/components/boardrooms/boardrooms.component.scss")).default]
    })
], BoardroomsComponent);



/***/ }),

/***/ "./src/app/components/chatrooms/chatrooms.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/components/chatrooms/chatrooms.component.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#logo {\n  position: absolute;\n  left: 50%;\n  transform: translateX(-50%);\n}\n#logo.boardroom-1 {\n  top: 10%;\n  left: 48%;\n}\n#logo.boardroom-2 {\n  top: 10%;\n  left: 48%;\n}\n#logo.boardroom-3 {\n  top: 30%;\n  left: 47%;\n}\n#logo.boardroom-4 {\n  top: 10%;\n  width: 260px;\n  height: 200px;\n  left: 23%;\n}\n#logo.boardroom-5 {\n  top: 12%;\n}\nbutton#back.boardroom-4 {\n  right: 65px;\n  left: auto;\n}\nbutton#watch.boardroom-1 {\n  left: auto;\n  right: 22%;\n  top: 30%;\n}\nbutton#watch.boardroom-2 {\n  left: 8%;\n}\nbutton#watch.boardroom-3 {\n  top: 40%;\n  left: 5.5%;\n}\nbutton#watch.boardroom-4 {\n  left: auto;\n  right: 10%;\n  top: 53%;\n}\nbutton#watch.boardroom-5 {\n  left: 4%;\n  top: 21%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uaWtrby92LXVuaXRlLXBsYXRmb3JtL3NyYy9hcHAvY29tcG9uZW50cy9jaGF0cm9vbXMvY2hhdHJvb21zLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL2NoYXRyb29tcy9jaGF0cm9vbXMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSwyQkFBQTtBQ0NGO0FEQUU7RUFDRSxRQUFBO0VBQ0EsU0FBQTtBQ0VKO0FEQUU7RUFDRSxRQUFBO0VBQ0EsU0FBQTtBQ0VKO0FEQUU7RUFDRSxRQUFBO0VBQ0EsU0FBQTtBQ0VKO0FEQUU7RUFDRSxRQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxTQUFBO0FDRUo7QURBRTtFQUNFLFFBQUE7QUNFSjtBRElJO0VBQ0UsV0FBQTtFQUNBLFVBQUE7QUNETjtBREtJO0VBQ0UsVUFBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0FDSE47QURLSTtFQUNFLFFBQUE7QUNITjtBREtJO0VBQ0UsUUFBQTtFQUNBLFVBQUE7QUNITjtBREtJO0VBQ0UsVUFBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0FDSE47QURLSTtFQUNFLFFBQUE7RUFDQSxRQUFBO0FDSE4iLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NoYXRyb29tcy9jaGF0cm9vbXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjbG9nb3tcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKTtcbiAgJi5ib2FyZHJvb20tMXtcbiAgICB0b3A6IDEwJTtcbiAgICBsZWZ0OiA0OCU7XG4gIH1cbiAgJi5ib2FyZHJvb20tMntcbiAgICB0b3A6IDEwJTtcbiAgICBsZWZ0OiA0OCU7XG4gIH1cbiAgJi5ib2FyZHJvb20tM3tcbiAgICB0b3A6IDMwJTtcbiAgICBsZWZ0OiA0NyU7XG4gIH1cbiAgJi5ib2FyZHJvb20tNHtcbiAgICB0b3A6IDEwJTtcbiAgICB3aWR0aDogMjYwcHg7XG4gICAgaGVpZ2h0OiAyMDBweDtcbiAgICBsZWZ0OiAyMyU7XG4gIH1cbiAgJi5ib2FyZHJvb20tNXtcbiAgICB0b3A6IDEyJTtcbiAgfVxufVxuXG5idXR0b257XG4gICYjYmFja3tcbiAgICAmLmJvYXJkcm9vbS00e1xuICAgICAgcmlnaHQ6IDY1cHg7XG4gICAgICBsZWZ0OiBhdXRvO1xuICAgIH1cbiAgfVxuICAmI3dhdGNoe1xuICAgICYuYm9hcmRyb29tLTF7XG4gICAgICBsZWZ0OiBhdXRvO1xuICAgICAgcmlnaHQ6IDIyJTtcbiAgICAgIHRvcDogMzAlO1xuICAgIH1cbiAgICAmLmJvYXJkcm9vbS0ye1xuICAgICAgbGVmdDogOCU7XG4gICAgfVxuICAgICYuYm9hcmRyb29tLTN7XG4gICAgICB0b3A6IDQwJTtcbiAgICAgIGxlZnQ6IDUuNSU7XG4gICAgfVxuICAgICYuYm9hcmRyb29tLTR7XG4gICAgICBsZWZ0OiBhdXRvO1xuICAgICAgcmlnaHQ6IDEwJTtcbiAgICAgIHRvcDogNTMlO1xuICAgIH1cbiAgICAmLmJvYXJkcm9vbS01e1xuICAgICAgbGVmdDogNCU7XG4gICAgICB0b3A6IDIxJTtcbiAgICB9XG4gIH1cbn1cbiIsIiNsb2dvIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKTtcbn1cbiNsb2dvLmJvYXJkcm9vbS0xIHtcbiAgdG9wOiAxMCU7XG4gIGxlZnQ6IDQ4JTtcbn1cbiNsb2dvLmJvYXJkcm9vbS0yIHtcbiAgdG9wOiAxMCU7XG4gIGxlZnQ6IDQ4JTtcbn1cbiNsb2dvLmJvYXJkcm9vbS0zIHtcbiAgdG9wOiAzMCU7XG4gIGxlZnQ6IDQ3JTtcbn1cbiNsb2dvLmJvYXJkcm9vbS00IHtcbiAgdG9wOiAxMCU7XG4gIHdpZHRoOiAyNjBweDtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgbGVmdDogMjMlO1xufVxuI2xvZ28uYm9hcmRyb29tLTUge1xuICB0b3A6IDEyJTtcbn1cblxuYnV0dG9uI2JhY2suYm9hcmRyb29tLTQge1xuICByaWdodDogNjVweDtcbiAgbGVmdDogYXV0bztcbn1cbmJ1dHRvbiN3YXRjaC5ib2FyZHJvb20tMSB7XG4gIGxlZnQ6IGF1dG87XG4gIHJpZ2h0OiAyMiU7XG4gIHRvcDogMzAlO1xufVxuYnV0dG9uI3dhdGNoLmJvYXJkcm9vbS0yIHtcbiAgbGVmdDogOCU7XG59XG5idXR0b24jd2F0Y2guYm9hcmRyb29tLTMge1xuICB0b3A6IDQwJTtcbiAgbGVmdDogNS41JTtcbn1cbmJ1dHRvbiN3YXRjaC5ib2FyZHJvb20tNCB7XG4gIGxlZnQ6IGF1dG87XG4gIHJpZ2h0OiAxMCU7XG4gIHRvcDogNTMlO1xufVxuYnV0dG9uI3dhdGNoLmJvYXJkcm9vbS01IHtcbiAgbGVmdDogNCU7XG4gIHRvcDogMjElO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/components/chatrooms/chatrooms.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/chatrooms/chatrooms.component.ts ***!
  \*************************************************************/
/*! exports provided: ChatroomsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatroomsComponent", function() { return ChatroomsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");



let ChatroomsComponent = class ChatroomsComponent {
    constructor(_sanitizer) {
        this._sanitizer = _sanitizer;
        this.heroImage = 'assets/chatrooms/1.jpg';
        this.scale = 1;
        this.vh = window.innerHeight;
        this.vw = window.innerWidth;
        this.boardroom = 1;
    }
    ngOnInit() {
    }
    onResize(event) {
        this.scale = Math.min(event.target.innerWidth / this.vw, event.target.innerHeight / this.vh);
        console.log(this.scale);
    }
    get transform() {
        return this._sanitizer.bypassSecurityTrustStyle("translate(-50%, -50%) scale(" + this.scale + ")");
    }
    changeBoardroom(number) {
        this.boardroom = number;
        this.heroImage = 'assets/chatrooms/' + number + '.jpg';
    }
};
ChatroomsComponent.ctorParameters = () => [
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:resize', ['$event'])
], ChatroomsComponent.prototype, "onResize", null);
ChatroomsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chatrooms',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./chatrooms.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/chatrooms/chatrooms.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./chatrooms.component.scss */ "./src/app/components/chatrooms/chatrooms.component.scss")).default]
    })
], ChatroomsComponent);



/***/ }),

/***/ "./src/app/components/entrance/entrance.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/components/entrance/entrance.component.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("span#logo {\n  left: 50%;\n  transform: translateX(-50%);\n  top: 15%;\n}\n\nbutton.main {\n  font-size: 70px;\n  margin: 0 auto;\n  display: block;\n  position: relative;\n  top: 50%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uaWtrby92LXVuaXRlLXBsYXRmb3JtL3NyYy9hcHAvY29tcG9uZW50cy9lbnRyYW5jZS9lbnRyYW5jZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvY29tcG9uZW50cy9lbnRyYW5jZS9lbnRyYW5jZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLFNBQUE7RUFDQSwyQkFBQTtFQUNBLFFBQUE7QUNBSjs7QURNRTtFQUNFLGVBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtBQ0hKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9lbnRyYW5jZS9lbnRyYW5jZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInNwYW57XG4gICYjbG9nb3tcbiAgICBsZWZ0OiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpO1xuICAgIHRvcDogMTUlO1xuICB9XG59XG5cblxuYnV0dG9ue1xuICAmLm1haW57XG4gICAgZm9udC1zaXplOiA3MHB4O1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6IDUwJTtcbiAgfVxufVxuIiwic3BhbiNsb2dvIHtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSk7XG4gIHRvcDogMTUlO1xufVxuXG5idXR0b24ubWFpbiB7XG4gIGZvbnQtc2l6ZTogNzBweDtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogNTAlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/components/entrance/entrance.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/entrance/entrance.component.ts ***!
  \***********************************************************/
/*! exports provided: EntranceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntranceComponent", function() { return EntranceComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");



let EntranceComponent = class EntranceComponent {
    constructor(_sanitizer) {
        this._sanitizer = _sanitizer;
        this.heroImage = 'assets/entrance-bg.jpg';
        this.scaleMode = true;
        this.scale = 1;
        this.vh = window.innerHeight;
        this.vw = window.innerWidth;
    }
    ngOnInit() {
    }
    onResize(event) {
        this.scale = Math.min(event.target.innerWidth / this.vw, event.target.innerHeight / this.vh);
        console.log(this.scale);
    }
    get transform() {
        return this._sanitizer.bypassSecurityTrustStyle("translate(-50%, -50%) scale(" + this.scale + ")");
    }
};
EntranceComponent.ctorParameters = () => [
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:resize', ['$event'])
], EntranceComponent.prototype, "onResize", null);
EntranceComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-entrance',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./entrance.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/entrance/entrance.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./entrance.component.scss */ "./src/app/components/entrance/entrance.component.scss")).default]
    })
], EntranceComponent);



/***/ }),

/***/ "./src/app/components/lobbys/lobbys.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/components/lobbys/lobbys.component.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#logo {\n  position: absolute;\n  left: 50%;\n  transform: translateX(-50%);\n}\n#logo.lobby-1 {\n  transform: translateX(-50%) skewY(-8deg);\n  top: 41.5%;\n  width: 9%;\n  left: 59.5%;\n}\n#logo.lobby-2 {\n  left: 22%;\n  top: 11%;\n}\n#logo.lobby-3 {\n  left: 22%;\n  top: 11%;\n}\n#logo.lobby-4 {\n  left: 22%;\n  top: 11%;\n}\nbutton#back.boardroom-4 {\n  right: 65px;\n  left: auto;\n}\nbutton#watch.boardroom-1 {\n  left: auto;\n  right: 22%;\n  top: 30%;\n}\nbutton#watch.boardroom-2 {\n  left: 8%;\n}\nbutton#watch.boardroom-3 {\n  top: 40%;\n  left: 5.5%;\n}\nbutton#watch.boardroom-4 {\n  left: auto;\n  right: 10%;\n  top: 53%;\n}\nbutton#watch.boardroom-5 {\n  left: 4%;\n  top: 21%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uaWtrby92LXVuaXRlLXBsYXRmb3JtL3NyYy9hcHAvY29tcG9uZW50cy9sb2JieXMvbG9iYnlzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL2xvYmJ5cy9sb2JieXMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSwyQkFBQTtBQ0NGO0FEQUU7RUFDRSx3Q0FBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtBQ0VKO0FEQUU7RUFDRSxTQUFBO0VBQ0EsUUFBQTtBQ0VKO0FEQUU7RUFDRSxTQUFBO0VBQ0EsUUFBQTtBQ0VKO0FEQUU7RUFDRSxTQUFBO0VBQ0EsUUFBQTtBQ0VKO0FESUk7RUFDRSxXQUFBO0VBQ0EsVUFBQTtBQ0ROO0FES0k7RUFDRSxVQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7QUNITjtBREtJO0VBQ0UsUUFBQTtBQ0hOO0FES0k7RUFDRSxRQUFBO0VBQ0EsVUFBQTtBQ0hOO0FES0k7RUFDRSxVQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7QUNITjtBREtJO0VBQ0UsUUFBQTtFQUNBLFFBQUE7QUNITiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbG9iYnlzL2xvYmJ5cy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNsb2dve1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpO1xuICAmLmxvYmJ5LTF7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpIHNrZXdZKC04ZGVnKTtcbiAgICB0b3A6IDQxLjUlO1xuICAgIHdpZHRoOiA5JTtcbiAgICBsZWZ0OiA1OS41JTtcbiAgfVxuICAmLmxvYmJ5LTJ7XG4gICAgbGVmdDogMjIlO1xuICAgIHRvcDogMTElO1xuICB9XG4gICYubG9iYnktM3tcbiAgICBsZWZ0OiAyMiU7XG4gICAgdG9wOiAxMSU7XG4gIH1cbiAgJi5sb2JieS00e1xuICAgIGxlZnQ6IDIyJTtcbiAgICB0b3A6IDExJTtcbiAgfVxufVxuXG5idXR0b257XG4gICYjYmFja3tcbiAgICAmLmJvYXJkcm9vbS00e1xuICAgICAgcmlnaHQ6IDY1cHg7XG4gICAgICBsZWZ0OiBhdXRvO1xuICAgIH1cbiAgfVxuICAmI3dhdGNoe1xuICAgICYuYm9hcmRyb29tLTF7XG4gICAgICBsZWZ0OiBhdXRvO1xuICAgICAgcmlnaHQ6IDIyJTtcbiAgICAgIHRvcDogMzAlO1xuICAgIH1cbiAgICAmLmJvYXJkcm9vbS0ye1xuICAgICAgbGVmdDogOCU7XG4gICAgfVxuICAgICYuYm9hcmRyb29tLTN7XG4gICAgICB0b3A6IDQwJTtcbiAgICAgIGxlZnQ6IDUuNSU7XG4gICAgfVxuICAgICYuYm9hcmRyb29tLTR7XG4gICAgICBsZWZ0OiBhdXRvO1xuICAgICAgcmlnaHQ6IDEwJTtcbiAgICAgIHRvcDogNTMlO1xuICAgIH1cbiAgICAmLmJvYXJkcm9vbS01e1xuICAgICAgbGVmdDogNCU7XG4gICAgICB0b3A6IDIxJTtcbiAgICB9XG4gIH1cbn1cbiIsIiNsb2dvIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKTtcbn1cbiNsb2dvLmxvYmJ5LTEge1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSkgc2tld1koLThkZWcpO1xuICB0b3A6IDQxLjUlO1xuICB3aWR0aDogOSU7XG4gIGxlZnQ6IDU5LjUlO1xufVxuI2xvZ28ubG9iYnktMiB7XG4gIGxlZnQ6IDIyJTtcbiAgdG9wOiAxMSU7XG59XG4jbG9nby5sb2JieS0zIHtcbiAgbGVmdDogMjIlO1xuICB0b3A6IDExJTtcbn1cbiNsb2dvLmxvYmJ5LTQge1xuICBsZWZ0OiAyMiU7XG4gIHRvcDogMTElO1xufVxuXG5idXR0b24jYmFjay5ib2FyZHJvb20tNCB7XG4gIHJpZ2h0OiA2NXB4O1xuICBsZWZ0OiBhdXRvO1xufVxuYnV0dG9uI3dhdGNoLmJvYXJkcm9vbS0xIHtcbiAgbGVmdDogYXV0bztcbiAgcmlnaHQ6IDIyJTtcbiAgdG9wOiAzMCU7XG59XG5idXR0b24jd2F0Y2guYm9hcmRyb29tLTIge1xuICBsZWZ0OiA4JTtcbn1cbmJ1dHRvbiN3YXRjaC5ib2FyZHJvb20tMyB7XG4gIHRvcDogNDAlO1xuICBsZWZ0OiA1LjUlO1xufVxuYnV0dG9uI3dhdGNoLmJvYXJkcm9vbS00IHtcbiAgbGVmdDogYXV0bztcbiAgcmlnaHQ6IDEwJTtcbiAgdG9wOiA1MyU7XG59XG5idXR0b24jd2F0Y2guYm9hcmRyb29tLTUge1xuICBsZWZ0OiA0JTtcbiAgdG9wOiAyMSU7XG59Il19 */");

/***/ }),

/***/ "./src/app/components/lobbys/lobbys.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/lobbys/lobbys.component.ts ***!
  \*******************************************************/
/*! exports provided: LobbysComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LobbysComponent", function() { return LobbysComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");



let LobbysComponent = class LobbysComponent {
    constructor(_sanitizer) {
        this._sanitizer = _sanitizer;
        this.heroImage = 'assets/lobbys/1.jpg';
        this.scale = 1;
        this.vh = window.innerHeight;
        this.vw = window.innerWidth;
        this.lobby = 1;
    }
    ngOnInit() {
    }
    onResize(event) {
        this.scale = Math.min(event.target.innerWidth / this.vw, event.target.innerHeight / this.vh);
        console.log(this.scale);
    }
    get transform() {
        return this._sanitizer.bypassSecurityTrustStyle("translate(-50%, -50%) scale(" + this.scale + ")");
    }
    changeBoardroom(number) {
        this.lobby = number;
        this.heroImage = 'assets/lobbys/' + number + '.jpg';
    }
};
LobbysComponent.ctorParameters = () => [
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('window:resize', ['$event'])
], LobbysComponent.prototype, "onResize", null);
LobbysComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-lobbys',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./lobbys.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/lobbys/lobbys.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./lobbys.component.scss */ "./src/app/components/lobbys/lobbys.component.scss")).default]
    })
], LobbysComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/nikko/v-unite-platform/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map